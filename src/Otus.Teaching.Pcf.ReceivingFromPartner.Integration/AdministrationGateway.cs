﻿using System;
using System.Net.Http;
using System.Threading.Tasks;
using Otus.Teaching.Pcf.ReceivingFromPartner.Core.Abstractions.Gateways;
using Otus.Teaching.Pcf.ReceivingFromPartner.Core.Domain;

namespace Otus.Teaching.Pcf.ReceivingFromPartner.Integration
{
    public class AdministrationGateway
        : IAdministrationGateway
    {
        
        public Task NotifyAdminAboutPartnerManagerPromoCode(Guid partnerManagerId)
        {
            return Task.CompletedTask;
        }
    }
}