﻿using System;

namespace Otus.Teaching.Pcf.ReceivingFromPartner.Core.Exceptions.Exceptions
{
    public class EntityValidationException
        : Exception
    {
        public EntityValidationException(string message)
            : base(message)
        {
            
        }
    }
}