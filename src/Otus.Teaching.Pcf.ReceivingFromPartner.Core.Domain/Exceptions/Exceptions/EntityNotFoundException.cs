﻿using System;

namespace Otus.Teaching.Pcf.ReceivingFromPartner.Core.Exceptions.Exceptions
{
    public class EntityNotFoundException
        : Exception
    {
        public EntityNotFoundException(string message)
            : base(message)
        {
            
        }
    }
}