﻿using System;

namespace Otus.Teaching.Pcf.ReceivingFromPartner.Core.Application.Dto
{
    public class ReceivePromoCodeFromPartnerWithPreferenceResponseDto
    {
        public Guid PartnerId { get; set; }

        public Guid PromoCodeId { get; set; }
    }
}