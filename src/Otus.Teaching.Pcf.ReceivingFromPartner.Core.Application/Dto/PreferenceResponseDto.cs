﻿using System;

namespace Otus.Teaching.Pcf.ReceivingFromPartner.Core.Application.Dto
{
    public class PreferenceResponseDto
    {
        public Guid Id { get; set; }
        
        public string Name { get; set; }
    }
}