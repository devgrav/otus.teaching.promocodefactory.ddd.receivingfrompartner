﻿using System;
using System.Collections.Generic;

namespace Otus.Teaching.Pcf.ReceivingFromPartner.Core.Application.Dto
{
    public class PartnerResponseDto
    {
        public Guid Id { get; set; }

        public bool IsActive { get; set; }
        
        public string Name { get; set; }

        public int NumberIssuedPromoCodes  { get; set; }

        public List<PartnerPromoCodeLimitResponseDto> PartnerLimits { get; set; }
    }
}