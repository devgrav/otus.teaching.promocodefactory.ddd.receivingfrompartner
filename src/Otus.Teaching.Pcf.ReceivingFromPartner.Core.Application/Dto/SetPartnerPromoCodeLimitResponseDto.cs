﻿using System;

namespace Otus.Teaching.Pcf.ReceivingFromPartner.Core.Application.Dto
{
    public class SetPartnerPromoCodeLimitResponseDto
    {
        public Guid PartnerId { get; set; }
        
        public Guid NewLimitId { get; set; }
    }
}