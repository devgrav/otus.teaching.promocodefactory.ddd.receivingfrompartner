﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Otus.Teaching.Pcf.ReceivingFromPartner.Core.Abstractions.Repositories;
using Otus.Teaching.Pcf.ReceivingFromPartner.Core.Application.Abstractions;
using Otus.Teaching.Pcf.ReceivingFromPartner.Core.Application.Dto;
using Otus.Teaching.Pcf.ReceivingFromPartner.Core.Domain;

namespace Otus.Teaching.Pcf.ReceivingFromPartner.Core.Application.Services
{
    public class PreferenceService
        : IPreferenceService
    {
        private readonly IRepository<Preference> _preferencesRepository;

        public PreferenceService(IRepository<Preference> preferencesRepository)
        {
            _preferencesRepository = preferencesRepository;
        }
        
        public async Task<List<PreferenceResponseDto>> GetPreferencesAsync()
        {
            var preferences = await _preferencesRepository.GetAllAsync();

            var response = preferences.Select(x => new PreferenceResponseDto()
            {
                Id = x.Id,
                Name = x.Name
            }).ToList();

            return response;
        }
    }
}