﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Otus.Teaching.Pcf.ReceivingFromPartner.Core.Abstractions.Gateways;
using Otus.Teaching.Pcf.ReceivingFromPartner.Core.Abstractions.Repositories;
using Otus.Teaching.Pcf.ReceivingFromPartner.Core.Application.Abstractions;
using Otus.Teaching.Pcf.ReceivingFromPartner.Core.Application.Dto;
using Otus.Teaching.Pcf.ReceivingFromPartner.Core.Application.Factories;
using Otus.Teaching.Pcf.ReceivingFromPartner.Core.Domain;
using Otus.Teaching.Pcf.ReceivingFromPartner.Core.Exceptions.Exceptions;

namespace Otus.Teaching.Pcf.ReceivingFromPartner.Core.Application.Services
{
    public class PartnerService
        : IPartnerService
    {
        private readonly IRepository<Partner> _partnersRepository;
        private readonly IRepository<Preference> _preferencesRepository;
        private readonly INotificationGateway _notificationGateway;
        private readonly IGivingPromoCodeToCustomerGateway _givingPromoCodeToCustomerGateway;
        private readonly IAdministrationGateway _administrationGateway;

        public PartnerService(IRepository<Partner> partnersRepository,
            IRepository<Preference> preferencesRepository, 
            INotificationGateway notificationGateway,
            IGivingPromoCodeToCustomerGateway givingPromoCodeToCustomerGateway,
            IAdministrationGateway administrationGateway)
        {
            _partnersRepository = partnersRepository;
            _preferencesRepository = preferencesRepository;
            _notificationGateway = notificationGateway;
            _givingPromoCodeToCustomerGateway = givingPromoCodeToCustomerGateway;
            _administrationGateway = administrationGateway;
        }
        
        public async Task<List<PartnerResponseDto>> GetPartnersAsync()
        {
            var partners = await _partnersRepository.GetAllAsync();

            var response = partners.Select(x => new PartnerResponseDto()
            {
                Id = x.Id,
                Name = x.Name,
                NumberIssuedPromoCodes = x.NumberIssuedPromoCodes,
                IsActive = true,
                PartnerLimits = x.PartnerLimits
                    .Select(y => new PartnerPromoCodeLimitResponseDto()
                    {
                        Id = y.Id,
                        PartnerId = y.PartnerId,
                        Limit = y.Limit,
                        CreateDate = y.CreateDate.ToString("dd.MM.yyyy hh:mm:ss"),
                        EndDate = y.EndDate.ToString("dd.MM.yyyy hh:mm:ss"),
                        CancelDate = y.CancelDate?.ToString("dd.MM.yyyy hh:mm:ss"),
                    }).ToList()
            }).ToList();

            return response;
        }

        public async Task<PartnerResponseDto> GetPartnersAsync(Guid id)
        {
            var partner = await _partnersRepository.GetByIdAsync(id);

            if (partner == null)
                throw new EntityNotFoundException("Партнер не найден");

            var response = new PartnerResponseDto()
            {
                Id = partner.Id,
                Name = partner.Name,
                NumberIssuedPromoCodes = partner.NumberIssuedPromoCodes,
                IsActive = true,
                PartnerLimits = partner.PartnerLimits
                    .Select(y => new PartnerPromoCodeLimitResponseDto()
                    {
                        Id = y.Id,
                        PartnerId = y.PartnerId,
                        Limit = y.Limit,
                        CreateDate = y.CreateDate.ToString("dd.MM.yyyy hh:mm:ss"),
                        EndDate = y.EndDate.ToString("dd.MM.yyyy hh:mm:ss"),
                        CancelDate = y.CancelDate?.ToString("dd.MM.yyyy hh:mm:ss"),
                    }).ToList()
            };

            return response;
        }

        public async Task<SetPartnerPromoCodeLimitResponseDto> SetPartnerPromoCodeLimitAsync(Guid id, SetPartnerPromoCodeLimitRequestDto requestDto)
        {
            var partner = await _partnersRepository.GetByIdAsync(id);

            if (partner == null)
                throw new EntityNotFoundException("Партнер не найден");
            
            //Если партнер заблокирован, то нужно выдать исключение
            if (!partner.IsActive)
                throw new EntityValidationException("Данный партнер не активен");
            
            //Установка лимита партнеру
            var activeLimit = partner.PartnerLimits.FirstOrDefault(x => 
                !x.CancelDate.HasValue);
            
            if (activeLimit != null)
            {
                //Если партнеру выставляется лимит, то мы 
                //должны обнулить количество промокодов, которые партнер выдал, если лимит закончился, 
                //то количество не обнуляется
                partner.NumberIssuedPromoCodes = 0;
                
                //При установке лимита нужно отключить предыдущий лимит
                activeLimit.CancelDate = DateTime.Now;
            }

            if (requestDto.Limit <= 0)
                throw new EntityValidationException("Лимит должен быть больше 0");

            var newLimit = new PartnerPromoCodeLimit()
            {
                Limit = requestDto.Limit,
                Partner = partner,
                PartnerId = partner.Id,
                CreateDate = DateTime.Now,
                EndDate = requestDto.EndDate
            };
            
            partner.PartnerLimits.Add(newLimit);

            await _partnersRepository.UpdateAsync(partner);
            
            await _notificationGateway
                .SendNotificationToPartnerAsync(partner.Id, "Вам установлен лимит на отправку промокодов...");

            return new SetPartnerPromoCodeLimitResponseDto()
            {
                PartnerId = partner.Id,
                NewLimitId = newLimit.Id
            };
        }

        public async Task<PartnerPromoCodeLimitResponseDto> GetPartnerLimitAsync(Guid id, Guid limitId)
        {
            var partner = await _partnersRepository.GetByIdAsync(id);

            if (partner == null)
                throw new EntityNotFoundException("Партнер не найден");
            
            var limit = partner.PartnerLimits
                .FirstOrDefault(x => x.Id == limitId);

            var response = new PartnerPromoCodeLimitResponseDto()
            {
                Id = limit.Id,
                PartnerId = limit.PartnerId,
                Limit = limit.Limit,
                CreateDate = limit.CreateDate.ToString("dd.MM.yyyy hh:mm:ss"),
                EndDate = limit.EndDate.ToString("dd.MM.yyyy hh:mm:ss"),
                CancelDate = limit.CancelDate?.ToString("dd.MM.yyyy hh:mm:ss"),
            };

            return response;
        }

        public async Task CancelPartnerPromoCodeLimitAsync(Guid id)
        {
            var partner = await _partnersRepository.GetByIdAsync(id);
            
            if (partner == null)
                throw new EntityNotFoundException("Данный партнер не активен");
            
            //Если партнер заблокирован, то нужно выдать исключение
            if (!partner.IsActive)
                throw new EntityValidationException("Данный партнер не активен");
            
            //Отключение лимита
            var activeLimit = partner.PartnerLimits.FirstOrDefault(x => 
                !x.CancelDate.HasValue);
            
            if (activeLimit != null)
            {
                activeLimit.CancelDate = DateTime.Now;
            }

            await _partnersRepository.UpdateAsync(partner);

            //Отправляем уведомление
            await _notificationGateway
                .SendNotificationToPartnerAsync(partner.Id, "Ваш лимит на отправку промокодов отменен...");
        }

        public async Task<List<PromoCodeShortResponseDto>> GetPartnerPromoCodesAsync(Guid id)
        {
            var partner = await _partnersRepository.GetByIdAsync(id);
            
            if (partner == null)
            {
                throw new EntityNotFoundException("Партнер не найден");
            }
            
            var response = partner.PromoCodes
                .Select(x => new PromoCodeShortResponseDto()
                {
                    Id = x.Id,
                    Code = x.Code,
                    BeginDate = x.BeginDate.ToString("yyyy-MM-dd"),
                    EndDate = x.EndDate.ToString("yyyy-MM-dd"),
                    PartnerName = x.Partner.Name,
                    PartnerId = x.PartnerId,
                    ServiceInfo = x.ServiceInfo
                }).ToList();

            return response;
        }

        public async Task<PromoCodeShortResponseDto> GetPartnerPromoCodeAsync(Guid id, Guid promoCodeId)
        {
            var partner = await _partnersRepository.GetByIdAsync(id);
            
            if (partner == null)
            {
                throw new EntityNotFoundException("Партнер не найден");
            }

            var promoCode = partner.PromoCodes.FirstOrDefault(x => x.Id == promoCodeId);

            if (promoCode == null)
            {
                throw new EntityNotFoundException("Партнер не найден");
            }
            
            var response =  new PromoCodeShortResponseDto()
            {
                Id = promoCode.Id,
                Code = promoCode.Code,
                BeginDate = promoCode.BeginDate.ToString("yyyy-MM-dd"),
                EndDate = promoCode.EndDate.ToString("yyyy-MM-dd"),
                PartnerName = promoCode.Partner.Name,
                PartnerId = promoCode.PartnerId,
                ServiceInfo = promoCode.ServiceInfo
            };

            return response;
        }

        public async Task<ReceivePromoCodeFromPartnerWithPreferenceResponseDto> ReceivePromoCodeFromPartnerWithPreferenceAsync(Guid id, ReceivingPromoCodeRequestDto requestDto)
        {
            var partner = await _partnersRepository.GetByIdAsync(id);
            
            if (partner == null)
            {
                throw new EntityNotFoundException("Партнер не найден");
            }

            var activeLimit = partner.PartnerLimits.FirstOrDefault(x
                => !x.CancelDate.HasValue && x.EndDate > DateTime.Now);

            if (activeLimit == null)
            {
                throw new EntityValidationException("Нет доступного лимита на предоставление промокодов");
            }

            if (partner.NumberIssuedPromoCodes + 1 > activeLimit.Limit)
            {
                throw new EntityValidationException("Лимит на выдачу промокодов превышен");
            }

            if (partner.PromoCodes.Any(x => x.Code == requestDto.PromoCode))
            {
                throw new EntityValidationException("Данный промокод уже был выдан ранее");
            }

            //Получаем предпочтение по имени
            var preference = await _preferencesRepository.GetByIdAsync(requestDto.PreferenceId);

            if (preference == null)
            {
                throw new EntityValidationException("Предпочтение не найдено");
            }

            PromoCode promoCode = PromoCodeFactory.MapFromModel(requestDto, preference, partner);
            partner.PromoCodes.Add(promoCode);
            partner.NumberIssuedPromoCodes++;

            await _partnersRepository.UpdateAsync(partner);
            
            //TODO: Чтобы информация о том, что промокод был выдан парнером была отправлена
            //в микросервис рассылки клиентам нужно либо вызвать его API, либо отправить событие в очередь
            await _givingPromoCodeToCustomerGateway.GivePromoCodeToCustomer(promoCode);

            //TODO: Чтобы информация о том, что промокод был выдан парнером была отправлена
            //в микросервис администрирования нужно либо вызвать его API, либо отправить событие в очередь

            if (requestDto.PartnerManagerId.HasValue)
            {
                await _administrationGateway.NotifyAdminAboutPartnerManagerPromoCode(requestDto.PartnerManagerId.Value);   
            }

            return new ReceivePromoCodeFromPartnerWithPreferenceResponseDto()
            {
                PartnerId = partner.Id,
                PromoCodeId = partner.Id
            };
        }
    }
}