﻿using System;
using Otus.Teaching.Pcf.ReceivingFromPartner.Core.Application.Dto;
using Otus.Teaching.Pcf.ReceivingFromPartner.Core.Domain;

namespace Otus.Teaching.Pcf.ReceivingFromPartner.Core.Application.Factories
{
    public class PromoCodeFactory
    {
        public static PromoCode MapFromModel(ReceivingPromoCodeRequestDto requestDto, Preference preference, Partner partner) {

            var promocode = new PromoCode();

            promocode.PartnerId = partner.Id;
            promocode.Partner = partner;
            promocode.Code = requestDto.PromoCode;
            promocode.ServiceInfo = requestDto.ServiceInfo;
           
            promocode.BeginDate = DateTime.Now;
            promocode.EndDate = DateTime.Now.AddDays(30);

            promocode.Preference = preference;
            promocode.PreferenceId = preference.Id;

            promocode.PartnerManagerId = requestDto.PartnerManagerId;

            return promocode;
        }
    }
}
