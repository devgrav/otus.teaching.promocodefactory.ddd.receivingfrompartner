﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Otus.Teaching.Pcf.ReceivingFromPartner.Core.Application.Dto;

namespace Otus.Teaching.Pcf.ReceivingFromPartner.Core.Application.Abstractions
{
    public interface IPreferenceService
    {
        Task<List<PreferenceResponseDto>> GetPreferencesAsync();
    }
}