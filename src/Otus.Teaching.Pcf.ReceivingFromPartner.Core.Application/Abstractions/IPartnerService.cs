﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Otus.Teaching.Pcf.ReceivingFromPartner.Core.Application.Dto;
using Otus.Teaching.Pcf.ReceivingFromPartner.Core.Domain;

namespace Otus.Teaching.Pcf.ReceivingFromPartner.Core.Application.Abstractions
{
    public interface IPartnerService
    {
        Task<List<PartnerResponseDto>> GetPartnersAsync();

        Task<PartnerResponseDto> GetPartnersAsync(Guid id);

        Task<SetPartnerPromoCodeLimitResponseDto> SetPartnerPromoCodeLimitAsync(Guid id, SetPartnerPromoCodeLimitRequestDto requestDto);

        Task<PartnerPromoCodeLimitResponseDto> GetPartnerLimitAsync(Guid id, Guid limitId);

        Task CancelPartnerPromoCodeLimitAsync(Guid id);

        Task<List<PromoCodeShortResponseDto>> GetPartnerPromoCodesAsync(Guid id);

        Task<PromoCodeShortResponseDto> GetPartnerPromoCodeAsync(Guid id, Guid promoCodeId);

        Task<ReceivePromoCodeFromPartnerWithPreferenceResponseDto> ReceivePromoCodeFromPartnerWithPreferenceAsync(Guid id,
            ReceivingPromoCodeRequestDto requestDto);
    }
}