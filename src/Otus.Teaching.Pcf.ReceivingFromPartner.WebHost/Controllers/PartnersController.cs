﻿﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
 using Otus.Teaching.Pcf.ReceivingFromPartner.Core.Abstractions.Gateways;
 using Otus.Teaching.Pcf.ReceivingFromPartner.Core.Abstractions.Repositories;
 using Otus.Teaching.Pcf.ReceivingFromPartner.Core.Application.Abstractions;
 using Otus.Teaching.Pcf.ReceivingFromPartner.Core.Application.Dto;
 using Otus.Teaching.Pcf.ReceivingFromPartner.Core.Domain;
 using Otus.Teaching.Pcf.ReceivingFromPartner.Core.Exceptions.Exceptions;

 namespace Otus.Teaching.Pcf.ReceivingFromPartner.WebHost.Controllers
{
    /// <summary>
    /// Партнеры
    /// </summary>
    [ApiController]
    [Route("api/v1/[controller]")]
    public class PartnersController
        : ControllerBase
    {
        private readonly IPartnerService _partnerService;

        public PartnersController(IPartnerService partnerService)
        {
            _partnerService = partnerService;
        }

        /// <summary>
        /// Получить список партнеров
        /// </summary>
        [HttpGet]
        public async Task<ActionResult<List<PartnerResponseDto>>> GetPartnersAsync()
        {
            return Ok(await _partnerService.GetPartnersAsync());
        }
        
        /// <summary>
        /// Получить информацию партнере
        /// </summary>
        /// <param name="id">Id партнера, например: <example>20d2d612-db93-4ed5-86b1-ff2413bca655</example></param>
        [HttpGet("{id:guid}")]
        public async Task<ActionResult<List<PartnerResponseDto>>> GetPartnersAsync(Guid id)
        {
            try
            {
                var response = await _partnerService.GetPartnersAsync(id);
                return Ok(response);
            }
            catch (EntityNotFoundException)
            {
                return NotFound();
            }
        }
        
        /// <summary>
        /// Установить лимит на промокоды для партнера
        /// </summary>
        [HttpPost("{id:guid}/limits")]
        public async Task<IActionResult> SetPartnerPromoCodeLimitAsync(Guid id, SetPartnerPromoCodeLimitRequestDto requestDto)
        {

            var responseDto = await _partnerService.SetPartnerPromoCodeLimitAsync(id, requestDto);
            
            return CreatedAtAction(nameof(GetPartnerLimitAsync), new 
                {id = responseDto.PartnerId, limitId = responseDto.NewLimitId}, null);
        }
        
        /// <summary>
        /// Получить лимит на промокоды для партнера
        /// </summary>
        /// <param name="id">Id партнера, например: <example>20d2d612-db93-4ed5-86b1-ff2413bca655</example></param>
        /// <param name="limitId">Id лимита партнера, например: <example>93f3a79d-e9f9-47e6-98bb-1f618db43230</example></param>
        [HttpGet("{id:guid}/limits/{limitId:guid}")]
        public async Task<ActionResult<PartnerPromoCodeLimitResponseDto>> GetPartnerLimitAsync(Guid id, Guid limitId)
        {
            try
            {
                var response = await _partnerService.GetPartnerLimitAsync(id, limitId);
            
                return Ok(response);
            }
            catch (EntityNotFoundException)
            {
                return NotFound();
            }
        }

        /// <summary>
        /// Отменить лимит на промокоды для партнера
        /// </summary>
        /// <param name="id">Id партнера, например: <example>0da65561-cf56-4942-bff2-22f50cf70d43</example></param>
        [HttpPost("{id:guid}/canceledLimits")]
        public async Task<IActionResult> CancelPartnerPromoCodeLimitAsync(Guid id)
        {
            await _partnerService.CancelPartnerPromoCodeLimitAsync(id);
            
            return NoContent();
        }
        
        /// <summary>
        /// Получить промокод партнера по id
        /// </summary>
        /// <returns></returns>
        [HttpGet("{id:guid}/promocodes")]
        public async Task<IActionResult> GetPartnerPromoCodesAsync(Guid id)
        {
            var response = await _partnerService.GetPartnerPromoCodesAsync(id);

            return Ok(response);
        }
        
        /// <summary>
        /// Получить промокод партнера по id
        /// </summary>
        /// <returns></returns>
        [HttpGet("{id:guid}/promocodes/{promoCodeId:guid}")]
        public async Task<IActionResult> GetPartnerPromoCodeAsync(Guid id, Guid promoCodeId)
        {
            var response = await _partnerService.GetPartnerPromoCodeAsync(id, promoCodeId);

            return Ok(response);
        }
        
        /// <summary>
        /// Создать промокод от партнера 
        /// </summary>
        /// <param name="id">Id партнера, например: <example>20d2d612-db93-4ed5-86b1-ff2413bca655</example></param>
        /// <param name="requestDto">Данные запроса/example></param>
        /// <returns></returns>
        [HttpPost("{id:guid}/promocodes")]
        public async Task<IActionResult> ReceivePromoCodeFromPartnerWithPreferenceAsync(Guid id,
            ReceivingPromoCodeRequestDto requestDto)
        {
            var response = await _partnerService.ReceivePromoCodeFromPartnerWithPreferenceAsync(id, requestDto);

            return CreatedAtAction(nameof(GetPartnerPromoCodeAsync), 
                new {id = response.PartnerId, promoCodeId = response.PromoCodeId}, null);
        }
    }
}